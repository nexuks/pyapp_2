var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');




var connection = mongoose.createConnection("mongodb://127.0.0.1:27017/pyapp");
autoIncrement.initialize(connection);


const OrderSchema = new Schema({
    orderId: Number,
    createdAt: { type: Date, default: new Date() },
    sender: String,
    receiver: { type: String, required: true },
    items: { type: String, required: true },

});

OrderSchema.pre('save', function (next) {
    console.log('saving new order...')
    next();
});

OrderSchema.post('save', function (order) {
    console.log('saved order: ', order);
});

OrderSchema.plugin(autoIncrement.plugin, {
    model: 'Order', field: 'orderId',
    startAt: 1, incrementBy: 1
});
module.exports = mongoose.model('Order', OrderSchema);