




$(document).ready(function () {
    $("#btnAddOrder").click(function () {
        var vsender = $("#sender").val();
        var vreceiver = $("#receiver").val();
        var vitems = $("#items").val();



        if (vitems == '' && vreceiver == '') {
            alert("Please fill out the form");
        } else {
            $.post("/api/order/add",
                {
                    sender: vsender,
                    receiver: vreceiver,
                    items: vitems
                },
                function (res, status) { // Required Callback Function

                    // alert("*----Received Data----*\n\nResponse : " + res + "\n\nStatus : " + status);
                    if (status == 'success') {
                        if (!res.error) {
                            $("#msg").text('SUCCESS!!!').css("color", "green");
                            $("#sender").val("");
                            $("#receiver").val("");
                            $("#items").val("");
                            alert('SUCCESS!!!!');
                        } else {
                            alert('FAILED!!! Error: ', res.error);
                            $("#msg").text('FAILED!!! Error: ', res.error).css("color", "red");
                        }
                    } else {
                        alert('FAILED!!! Error client: ', status);
                        $("#msg").text('FAILED!!! Error client: ', status).css("color", "red");
                    }
                });


            // console.log('vsender:', vsender);
            // console.log('vreceiver:', vreceiver);
            // console.log('vitems:', vitems);
        }
    });
});
