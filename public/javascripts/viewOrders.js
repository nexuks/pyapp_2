
$(document).ready(function () {
    $('#datepicker')
        .datepicker({
            format: 'dd - M - yyyy',
            todayHighlight: true,
            autoclose: true,
        })
        .val(moment(new Date()).format('DD - MMM - YYYY'))
        .on('changeDate', function (e) {
            const newDate = e.date;
            getOrderList(newDate);
        });

    //init table
    $('#table').DataTable({
        // dom: "Bfrtip",
        data: [],
        columns: [
            { title: "date", data: 'createdAt' },
            { title: "Sender", data: "sender" },
            { title: "Receiver", data: 'receiver' },
            { title: "Items", data: 'items' }
        ],
        // select: true,
        responsive: true,
        // buttons: [
        //     'selectAll',
        //     'selectNone',
        //     {
        //         extend: 'selected',
        //         text: 'Print',
        //         action: function (e, dt, button, config) {
        //             console.log(dt.rows({ selected: true }).data().toArray());
        //             const data = dt.rows({ selected: true }).data().toArray();
        //             // alert(dt.rows({ selected: true }).indexes().length + ' row(s) selected');

        //             // alert(dt.rows('.selected').data().length + ' row(s) selected');
        //             printSelectedOrder(data);
        //         }
        //     },

        // ]
    });

    $('#table').on('page.dt', function () {
        const table = $('#table ').DataTable();
        $('#table > tbody  > tr').each(function () {
            $(this).removeClass('selected');
        });
        var info = table.page.info();
        $('#pageInfo').html('Showing page: ' + info.page + ' of ' + info.pages);
    });

    $('#table tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
    });

    $('#selectAllBtn').click(function () {
        $('#table > tbody  > tr').each(function () {
            $(this).addClass('selected');
        });
    });
    $('#selectNoneBtn').click(function () {
        $('#table > tbody  > tr').each(function () {
            $(this).removeClass('selected');
        });
    });
    $('#printBtn').click(function () {
        const table = $('#table ').DataTable();
        const data = table.rows('.selected').data().toArray();
        printSelectedOrder(data);
    });
    $('#deleteBtn').click(function () {
        const table = $('#table ').DataTable();
        const data = table.rows('.selected').data().toArray();
        if (data.length > 0) {
            deleteOrder(data);
        }
    });

    // $('#table tbody').on('click', 'tr', function () {
    //     $(this).toggleClass('.tableSelected');

    //     const table = $('#table').DataTable();
    //     const row = table.rows('.tableSelected');
    //     row.select();
    // });


    getOrderList(new Date());
});


function deleteOrder(data) {
    $('#loading').fadeIn();
    $('#msg').fadeOut();

    const deleteOrdersId = [];
    data.forEach(function (order) {
        deleteOrdersId.push(order._id);
    });

    $.post("/api/order/delete",
        {
            ids: deleteOrdersId
        },
        function (res, status) {

            if (status == 'success') {
                if (!res.error) {
                    const date = $('#datepicker').datepicker('getDate');
                    getOrderList(date);
                } else {
                    $('#tableDiv').fadeOut();
                    $("#msg").text('FAILED!!! Error: ', res.error).css("color", "red").fadeIn();

                }
            } else {
                $('#tableDiv').fadeOut();
                $("#msg").text('FAILED!!! Error client: ', status).css("color", "red").fadeIn();

            }
        });

};


function getOrderList(date) {
    $('#loading').fadeIn();
    $('#msg').fadeOut();

    $.post("/api/order/list",
        {
            date: date
        },
        function (res, status) { // Required Callback Function

            // alert("*----Received Data----*\n\nResponse : " + res + "\n\nStatus : " + status);
            if (status == 'success') {
                if (!res.error) {
                    const dataSets = [];
                    res.orders.forEach(order => {
                        order.createdAt = moment(order.createdAt).format('DD - MMM - YYYY HH:mm ');
                    });
                    const table = $('#table').DataTable();
                    table.clear().rows.add(res.orders).draw();
                    // table.clear().draw();


                    $('#loading').fadeOut();
                    $('#tableDiv').fadeIn();

                } else {
                    $('#tableDiv').fadeOut();
                    $("#msg").text('FAILED!!! Error: ', res.error).css("color", "red").fadeIn();
                }
            } else {
                $('#tableDiv').fadeOut();
                $("#msg").text('FAILED!!! Error client: ', status).css("color", "red").fadeIn();
            }
        });
};


function printSelectedOrder(data) {

    const pagesContent = genPage(data);
    pdfMake.fonts = {
        'chinese.simfang': {
            normal: 'chinese.simfang.ttf',
            bold: 'chinese.simfang.ttf',
            italics: 'chinese.simfang.ttf',
            bolditalics: 'chinese.simfang.ttf'
        }
    };
    var docDefinition = {

        pageSize: { width: 595, height: 280 },
        pageOrientation: 'landscape',
        pageMargins: [10, 50, 20, 40],

        content: pagesContent,
        defaultStyle: {
            font: 'chinese.simfang'
        },
        styles: {
            acount_no: {
                fontSize: 15,
                bold: true
            },
            normal: {
                fontSize: 12,
                alignment: 'left'
            },
            orderid: {
                fontSize: 12,
                alignment: 'right'
            }
        }
    };

    const newPdf = pdfMake.createPdf(docDefinition)
    // newPdf.open();
    const date = moment(Date.now()).format('DD_MM_YYYY_HH:mm:ss');
    newPdf.download('order_' + date + '.pdf');


};


function genPage(data) {

    const pages = [];
    data.forEach(function (d, index, array) {

        const defaultSenderName = 'SITI NUR 016-8887361'
        const defaultSenderAddress = '\nC-8-17 Fortune Avenue\nJalan Metro Perdana 2 Taman Usahawan\n52100 Kepong K.L.';
        let sender = d.sender ? d.sender : defaultSenderName;
        sender = sender + defaultSenderAddress;
        const newLineLength = sender.split('\n').length;
        if (newLineLength < 5) {
            for (let i = newLineLength; i < 6; i++) {
                sender = sender.concat(" \n");
            }
        }
        const receiver = d.receiver;
        const items = d.items;
        const id = d._id;

        const newPage = {
            columns: [
                {
                    width: '50%',
                    stack: [

                        { text: '8800 459 673', style: 'acount_no', margin: [80, 5, 0, 0] },

                        { text: sender, style: 'normal', margin: [50, 8, 0, 0] },

                        { text: receiver, style: 'normal', margin: [50, 12, 0, 0] }
                    ]
                }, {
                    stack: [
                        // { text: '-' + id + '-', style: 'orderid', margin: [0, 10, 50, 0] },
                        { text: items, style: 'normal', margin: [0, 60, 0, 0] }
                    ]
                }

            ],
            columnGap: 10,
        };

        if (index !== array.length - 1) {
            newPage.pageBreak = 'after';
        }

        pages.push(newPage);
    });
    return pages;


}